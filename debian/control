Source: ruby-rexml
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Daniel Leidert <dleidert@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby-test-unit
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-rexml.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-rexml
Homepage: https://github.com/ruby/rexml
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-rexml
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: XML toolkit for Ruby
 REXML was inspired by the Electric XML library for Java, which features an
 easy-to-use API, small size, and speed. Hopefully, REXML, designed with the
 same philosophy, has these same features. It supports both tree and stream
 document parsing. Stream parsing is faster (about 1.5 times as fast). However,
 with stream parsing, one doesn't get access to features such as XPath.
 .
 Versions of this gem are also shipped with libruby2.x.
